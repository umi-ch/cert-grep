# John's Agile Cert Grep

## Goal
Given a web site domain, eg: &nbsp; www.brack.ch
Implement a script in **Python** that performs the following tasks:

1. Get SSL Certificate Information from the implicit URL:<br/>
  `Subject, Issuer, SAN, Validity`

2. Perform a GET request towards **Fortiguard** to fetch the web category of the given website URL<br/>
  `GET https://fortiguard.com/webfilter?q=URL`

3. Once this info is collected, pretty print it to Pastebin (using the API),<br/>
   and provide the URL as output of the script.

4. (Bonus): Perform some (automated) security relevant analysis on the TLS connection.

Example: &nbsp; `$ ./cert_info.py  www.brack.ch`<br/>
Output: &nbsp; `https://pastebin.com/i3sUvR51`

In my implementation, a **Linux Bash shell** launches the Python application<br/>
which writes to **standard output**, because Pastebin is not a reliable service.

&nbsp;

## Inspiration: OpenSSL in a Bash shell
Here are Bash convenience functions and aliases **ssl_grep** , **cert_grep**
showing SSL/TLS certificate info.<br/>
This business-logic is reimplemented in a Python script below, to batch-process lists of web sites.

```shell
$ source bashers

$ ssl_grep https://www.brack.ch
-> www.brack.ch
-$ openssl s_client -connect www.brack.ch:443 -servername www.brack.ch -showcerts < /dev/null 2> /tmp/q_ssl_grep.JUspJg  |  f_cert_grep

0: Certificate:
  Public-Key: (2048 bit)
  Issuer:   C=US, O=DigiCert Inc, OU=www.digicert.com, CN=GeoTrust RSA CA 2018
  Subject:  C=CH, ST=Aargau, L=M\xC3\xA4genwil, O=Competec Holding AG, OU=IT, CN=cdn.competec.ch
  Validity:
     Not Before: Mar 16 00:00:00 2020 GMT
     Not After : Mar 27 12:00:00 2021 GMT
  Data:
     Version: 3 (0x2)
     Serial Number: 02:f3:50:4f:c0:06:20:2a:eb:53:e3:f7:2d:b9:5e:50
  ...
  X509v3 Subject Alternative Name:
     DNS:cdn.competec.ch
     DNS:www.brack.ch
     DNS:beta.brack.ch
     DNS:old.alltron.ch
     DNS:assets.brack.ch
     DNS:www.alltron.ch
  X509v3 Extended Key Usage:
     TLS Web Server Authentication
     TLS Web Client Authentication

1: Certificate:
  Public-Key: (2048 bit)
  Issuer:   C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert Global Root CA
  Subject:  C=US, O=DigiCert Inc, OU=www.digicert.com, CN=GeoTrust RSA CA 2018
  Validity:
     Not Before: Nov  6 12:23:45 2017 GMT
     Not After : Nov  6 12:23:45 2027 GMT
  Data:
     Version: 3 (0x2)
     Serial Number: 05:46:fe:18:23:f7:e1:94:1d:a3:9f:ce:14:c4:61:73
  ...
  X509v3 Extended Key Usage:
     TLS Web Server Authentication
     TLS Web Client Authentication

2: Certificate:
  Public-Key: (2048 bit)
  Issuer:   C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert Global Root CA
  Subject:  C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert Global Root CA
  Validity:
     Not Before: Nov 10 00:00:00 2006 GMT
     Not After : Nov 10 00:00:00 2031 GMT
  Data:
     Version: 3 (0x2)
     Serial Number: 08:3b:e0:56:90:42:46:b1:a1:75:6a:c9:59:91:c7:4a
```



&nbsp;

## Implementation: Python

### Task 1
Python app **cert_info.py** shows output to the Bash function above -- that is,
nicely formatted STDOUT text. It can process one or more domains from
the command-line, or read a list from a file.

### Task 2

Nicely formatted output also appears when gathering info from
**Fortinet** including error-messages which are trapped and skipped.
See the source-code for implementation notes about OpenSSL versions.

### Task 3

**--Deprecated--**<br/>
Optional script parameter **-p** writes a copy of the output
to the **PasteBin** web site.

Although this is a main task criteria, it's
implemented as an optional feature since that web site only allows a small
number of updates before requiring Captcha access, and only a few more before
the daily limit is reached.

Furthermore, commercial subscriptions are no longer
provided at this time, so it's not well suited for for application development.

https://pastebin.com/qGqrzmvA


### Task 4

Security analysis is provided regarding the Certificate such as "is expired?".

Regarding the SSL/TLS connection itself, this is not provided at the time as it
requires a deeper dive into the various versions of the Python socket library
and O/S platforms.

This Python app is currently multi-platfrom (MacOS, several Linuxes via Docker
images, OpenSSL version) so getting this network info is still a work in progress.

However, the script implements two modes: **safe** (default) and **allow unsafe**
(option **-u**). Invalid certificates (eg: self-signed, expired) are rejected
in **safe** mode, but **unsafe** analysis, information about these web sites
can still be obtained.

There is no security risk here; "unsafe" simply refers to the assumed situation
with standard web browsing.

### Output

Output is written to the sub-directory `./output/` for convenient artifact bundling
with **GitLab Pipelines**.

For diagnostics, the last HTML output from **Fortinet** is also written to a file,
along with **JSON** output which is very useful these days, and comes "almost for free"
with Python.

The main challenge with the app is correctly handling all Unicode opportunities
which can be system-dependent regarding the O/S platform on which it runs.

&nbsp;

## Script info

```shell
$ ./cert-info.py  --version
cert-info.py (2.0.0)

$ ./cert-info.py  --help
usage: cert-info.py [-h] [-f FILE] [-o OUTPUT] [-p] [-u] [-V] [-v]
                    [domain [domain ...]]

positional arguments:
  domain                domains to check

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  file with domains to check
  -o OUTPUT, --output OUTPUT
                        output directory, defaults to ./output/
  -p, --pastebin        send results to PastBin web via API, using
                        PASTEBIN_KEY environment variable.
  -u, --unsafe          allow unsafe SSL, eg: self-signed certificates.
  -V, --version         show script version
  -v, --verbose         verbose level... repeat up to three times.

 Show info about X.509 certificates of web domains, and Fortiguard webfilter cataegory.
  Results can be sent to PasteBin web site, if PASTEBIN_KEY is set correctly.
  Raw JSON results of the certificate lookup are written here:  output/results.json
```

&nbsp;

## Demo: Safe mode

Invalid certificates (eg: self-signed, expired) are rejected in **safe** mode.

```shell
$ ./cert-info.py  www.brack.ch  www.brack.xxx  www.jdb.ch  www.google.com
- cert-info.py (2.0.0)
- SAFE mode is active.

www.brack.ch
  Subject:    C=CH, CN=cdn.competec.ch, L=Mägenwil, O=Competec Holding AG, OU=IT, ST=Aargau
  Issuer:     C=US, CN=GeoTrust RSA CA 2018, O=DigiCert Inc, OU=www.digicert.com
  notBefore:  2020-03-16 00:00:00 Z
  notAfter:   2021-03-27 12:00:00 Z
  SAN:        DNS:assets.brack.ch
              DNS:beta.brack.ch
              DNS:old.alltron.ch
              DNS:www.alltron.ch
              DNS:www.brack.ch
              DNS:cdn.competec.ch
  ** Analysis
    Expired?         No  [OK]
    Purpose overlap? Yes [NOK]  -> extended_Key_Usage: TLS Web Server Authentication, TLS Web Client Authentication
    Self-signed?     No  [OK]

- Fortiguard
  Category:   Shopping

www.brack.xxx
- socket.error:
  [Errno -2] Name or service not known

www.google.com
  Subject:    C=US, CN=www.google.com, L=Mountain View, O=Google LLC, ST=California
  Issuer:     C=US, CN=GTS CA 1O1, O=Google Trust Services
  notBefore:  2020-05-26 15:30:03 Z
  notAfter:   2020-08-18 15:30:03 Z
  SAN:        DNS:www.google.com
  ** Analysis
    Expired?         No  [OK]
    Purpose overlap? No  [OK]
    Self-signed?     No  [OK]

- Fortiguard
  Category:   Search Engines and Portals

www.jdb.ch
- UNSAFE exception SSLError:
  -> Try flag -u to override.
  [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:877)

- PasteBin
  -> Skipped.
```

&nbsp;

## Demo: Unsafe mode

Invalid certificates (eg: self-signed, expired) are accepted and processed in **unsafe** mode.<br/>

FYI: There is **no security risk** here.<br/>
"Unsafe mode" simply reflects common browser behavior, wanting to avoid such web sites.<br/>


```shell
$ ./cert-info.py -u  www.brack.ch  www.brack.xxx  www.jdb.ch  www.google.com
- cert-info.py (2.0.0)
- UNSAFE mode is active.

www.brack.ch
  Subject:    C=CH, CN=cdn.competec.ch, L=Mägenwil, O=Competec Holding AG, OU=IT, ST=Aargau
  Issuer:     C=US, CN=GeoTrust RSA CA 2018, O=DigiCert Inc, OU=www.digicert.com
  notBefore:  2020-03-16 00:00:00 Z
  notAfter:   2021-03-27 12:00:00 Z
  SAN:        DNS:assets.brack.ch
              DNS:beta.brack.ch
              DNS:old.alltron.ch
              DNS:www.alltron.ch
              DNS:www.brack.ch
              DNS:cdn.competec.ch
  ** Analysis
    Expired?         No  [OK]
    Purpose overlap? Yes [NOK]  -> extended_Key_Usage: TLS Web Server Authentication, TLS Web Client Authentication
    Self-signed?     No  [OK]

- Fortiguard
  Category:   Shopping

www.brack.xxx
- socket.error:
  [Errno -2] Name or service not known

www.google.com
  Subject:    C=US, CN=www.google.com, L=Mountain View, O=Google LLC, ST=California
  Issuer:     C=US, CN=GTS CA 1O1, O=Google Trust Services
  notBefore:  2020-05-26 15:30:03 Z
  notAfter:   2020-08-18 15:30:03 Z
  SAN:        DNS:www.google.com
  ** Analysis
    Expired?         No  [OK]
    Purpose overlap? No  [OK]
    Self-signed?     No  [OK]

- Fortiguard
  Category:   Search Engines and Portals

www.jdb.ch
  Subject:    C=DE, CN=webserver.ispgateway.de, L=Muenchen, O=ispgateway, ST=Bayern, emailAddress=hostmaster@ispgateway.de
  Issuer:     C=DE, CN=webserver.ispgateway.de, L=Muenchen, O=ispgateway, ST=Bayern, emailAddress=hostmaster@ispgateway.de
  notBefore:  2010-10-11 14:28:32 Z
  notAfter:   2020-10-08 14:28:32 Z
  SAN:        - None -
  ** Analysis
    Expired?         No  [OK]
    Purpose overlap? No  [OK]
    Self-signed?     Yes [NOK]

- Fortiguard
  Category:   Business

- PasteBin
  -> Skipped.
```

&nbsp;

## GitOps : GitLab CI/CD job runners

In addition to the common model of "clone this repo, then run it on your own system",<br/>
you can also run the demo scripts directly here in **GitLab**.<br/>

If you upload a clone of this repository into your own **GitLab** instance,<br/>
then have you own web-based certificate checking system.

The **GitLab runners** are configured in file `.gitlab-ci.yml`<br/>
I define several **manually triggered** batch-jobs:

Stage | Description
---|---
run_small  |  Show Docker image info, then run **cert-info** with a few ad-hoc domains.
run_medium  |  Run **cert-info** with a few domains listed in a file.
run_bad_ssl |  Run **cert-info** with safely-prepared "bad examples": &nbsp; https://badssl.com
run_many  |  Run **cert-info** with a large list of domains in a file, > 1 hour runtime.
run_small_2020  |  Earlier 2020 edition of run_small.

FYI:<br/>
I use other people's **Docker** images on Docker hub, which can change over time.<br/>
Updates may then be required to this code and CI/CD job definitions.

FYI:<br/>
**GitLab runners** may timeout after one hour.<br/>
`run_many_2022` typically exceeds this limit; run it on your own server instead.

<br/>

### CI/CD Editor

This section shows file `.gitlab-ci.yml`, for easy editing and development.

[<img src="./zPix/image-1.jpg" width="800" />](./zPix/image-1.jpg)

<br/>

### CI/CD Pipeline

This shows several ways to manually launch a **GitLab runner**.<br/>
Eg: **Run medium 2022**

[<img src="./zPix/image-2.jpg" width="800" />](./zPix/image-2.jpg)

<br/>

### Click here to open a console into a running job.

The icon of "circled crescent moon" is your friend.

[<img src="./zPix/image-3.jpg" width="800" />](./zPix/image-3.jpg)

<br/>

### Trigger button

If you see a screen with this button, press it: &nbsp; **Trigger this manual action**

[<img src="./zPix/image-4.jpg" width="800" />](./zPix/image-4.jpg)

<br/>

### Console

This shows output from the Bash shell launch commands, and the Python script.

[<img src="./zPix/image-5.jpg" width="800" />](./zPix/image-5.jpg)

<br/>

### Errors

Error messages from a **GitLab job runner** will appear here.

[<img src="./zPix/image-6.jpg" width="800" />](./zPix/image-6.jpg)

<br>


## Agile theme
This repository has useful working examples, quickly and iteratively adaptable to new environments.
<br/>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>
