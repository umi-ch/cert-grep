#!/usr/bin/env python3
#
##-No: #!/usr/bin/python3

# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.


"""
Script to get Certificate info about web sites.
https://stackoverflow.com/questions/7689941/how-can-i-retrieve-the-tls-ssl-peer-certificate-of-a-remote-host-using-python
https://www.crummy.com/software/BeautifulSoup/bs4/doc/#
https://www.pyopenssl.org/en/stable/

FYI: These Python modules are required:
$ pip install python-dateutil requests pyopenssl beautifulsoup4

FYI:
This site has a safe collection of bad examples, for testing:
    https://badssl.com/
"""

# - Semantic Versioning:
#   https://semver.org/
#
__version__ = "2.0.0"


# - Mind the ordering:
import argparse
import datetime
import json
import os
import pprint
import re
import socket
import ssl
import sys
import time

from bs4 import BeautifulSoup
import requests
import OpenSSL


# - Template for elapsed time counter:
start_time = time.time()

# - PasteBin stuff
pasteBin_url = 'https://pastebin.com/api/api_post.php'
wText = ''      # - text for PasteBin

# - Sleep and retry intervals
sleep_loop  = 1     # - Seconds between iterations, to avoid pounding web sites.



###########
# Methods #
###########

def parseArgs():
    ##-Not-Used: global args

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=" " +
        "Show info about X.509 certificates of web domains, and Fortiguard webfilter cataegory.\n" +
        "  Results can be sent to PasteBin web site, if PASTEBIN_KEY is set correctly.\n" +
        "  Raw JSON results of the certificate lookup are written here:  output/results.json\n" +
        "\n" +
        " Example:\n" +
        "   $ ./cert-info.py  www.brack.ch\n" +
        " \n")

    parser.add_argument(
        '-f', '--file',
        help='file with domains to check')

    parser.add_argument(
        'domain',
        nargs='*',
        help='domains to check')

    parser.add_argument(
        '-o', '--output',
        default='./output',
        help='output directory, defaults to ./output/')

    parser.add_argument(
        '-p', '--pastebin',
        action='store_true',
        help='send results to PastBin web via API, using PASTEBIN_KEY environment variable.')

    parser.add_argument(
        '-u', '--unsafe',
        action='store_true',
        help='allow unsafe SSL, eg: self-signed certificates.')

    parser.add_argument(
        '-V', '--version',
        action='version',
        help='show script version',
        version="%(prog)s ("+__version__+")")

    parser.add_argument(
        '-v', '--verbose',
        action='count',
        default=0,
        help="verbose level... repeat up to three times.")

    return parser.parse_args(sys.argv[1:])


def loadList(domainsFile, domains):
    global args

    if not domains:
        domains = []
    if domainsFile:
        with open(domainsFile, 'r') as f:
            for domain in f:
                domain = domain.strip()
                if domain.startswith('#'):
                    continue
                if not domain:
                    continue
                domains.append(domain)

    return domains



# - Get the Certificate info for a domain
#
def get_certificate(host, port=443, timeout=5):
    global args, wText

    context = None
    my_ssl_style = 3    # - Selector to test Python SSL updates and incompatibilities

    assert my_ssl_style in [1, 2, 3, 4],  f"Error, my_ssl_style has a bad value: {my_ssl_style}"

    if args.unsafe:
        # - Here be dragons:
        #   https://docs.python.org/3/library/ssl.html#ssl-security

        if my_ssl_style == 1:
            context = ssl.SSLContext()                  # - Works in Python <= v3.6
            ssl.SSLContext( ssl.PROTOCOL_TLS_CLIENT )

        elif my_ssl_style == 2:
            # - The construct above can give a new error/warning message, in 2022:
            #   cert-info.py:137: DeprecationWarning:
            #       ssl.SSLContext() without protocol argument is deprecated.
            #       context = ssl.SSLContext()
            #
            # - Use this invocation instead:
            #   https://stackoverflow.com/questions/73570775/ssl-protocol-tlsv1-2-is-deprecated-so-what-should-i-do
            #   https://docs.python.org/3/library/ssl.html#ssl-contexts
            #   https://docs.python.org/3/library/ssl.html#ssl.PROTOCOL_TLS
            #
            #   "Deprecated since version 3.10:
            #    SSLContext without protocol argument is deprecated.
            #    The context class will either require PROTOCOL_TLS_CLIENT or PROTOCOL_TLS_SERVER
            #    protocol in the future.
            #   "
            #
            # - Regardless of the TLS version selected, we are also limited by OpenSSL verison,
            #   and the options it was compiled with.

            ## context = ssl.SSLContext( ssl.PROTOCOL_TLS )         # - Deprecated
            context = ssl.SSLContext( ssl.PROTOCOL_TLS_CLIENT )     # - Backwards compatible?

            ## context.minimum_version = ssl.TLSVersion.TLSv1       # - Needed here?
            ## context.maximum_version = ssl.TLSVersion.TLSv1_3     #   We're tolerant.

        elif my_ssl_style == 3:
            # https://stackoverflow.com/questions/69453013/python-ssl-certificate-verify-failed-certificate-verify-failed-unable-to-g
            # - Ignore SSL certificate errors
            #
            context = ssl.create_default_context()
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE

        elif my_ssl_style == 4:
            # https://stackoverflow.com/questions/69453013/python-ssl-certificate-verify-failed-certificate-verify-failed-unable-to-g
            # - or:
            #
            ssl._create_default_https_context = ssl._create_unverified_context
            context = ssl.create_default_context()

        else:
            print("- Error, my_ssl_style has a bad value:" + str(my_ssl_style) + "\n")
            sys.exit(1)        ## raise RuntimeError()


        # - Allow bad stuff, like self-signed certificates, and check them later.
        #   https://docs.python.org/3/library/ssl.htm
        #
        ## context.options &= ~ssl.OP_NO_SSLv3
        context.check_hostname = False

    else:
        # - Default context should be safe. (for some definitions of "safe" :-)
        context = ssl.create_default_context()


    # - Extract the port from the hostname, if provided. Eg:
    #   tls-v1-1.badssl.com         ->  host: tls-v1-1.badssl.com , port: 443  (default)
    #   tls-v1-1.badssl.com:1011    ->  host: tls-v1-1.badssl.com , port: 1011

    parts = host.partition(':')     # Eg: tls-v1-1.badssl.com:1011
    if parts[2]:
        host = parts[0]             # Eg: tls-v1-1.badssl.com
        port = int(parts[2])        # Eg: 1011
    ## print ("- host: '" + host + "', port: '" + str(port) + "'")


    try:
        conn = socket.create_connection((host, port), timeout)
    except socket.error as ex:
        # - "socket.gaierror : [Errno 8] nodename nor servname provided, or not known"
        print("- socket.error:\n  {0}".format(ex))
        wText += "- socket.error:\n  {0}".format(ex) + "\n"
        raise RuntimeError()

    try:
        sock = context.wrap_socket(conn, server_hostname=host)

    # - Not all OpenSSL versions have this exception, eg CentOS 7 Docker images used by GitLab.
    #   The generic exception below (sss.SSLError) seems to work for this failure use-case.
    #
    ## except ssl.SSLCertVerificationError as ex:
    ##    print("- UNSAFE exception: SSLCertVerificationError\n  -> Use flag -u to override.\n  {0}".format(ex))
    ##    wText += "- UNSAFE exception: SSLCertVerificationError\n  -> Use flag -u to override.\n  {0}".format(ex) + "\n"
    ##    raise RuntimeError()

    except ssl.SSLError as ex:
        print("- UNSAFE exception SSLError:\n  -> Try flag -u to override.\n  {0}".format(ex))
        wText += "- UNSAFE exception SSLError:\n  -> Try flag -u to override.\n  {0}".format(ex) + "\n"
        raise RuntimeError()

    except ConnectionResetError as ex:
        print("- REMOTE exception: ConnectionResetError:\n  {0}".format(ex))
        wText += "- REMOTE exception: ConnectionResetError:\n  {0}".format(ex) + "\n"
        raise RuntimeError()

    except socket.timeout as ex:
        print("- REMOTE exception: Timeout:\n  {0}".format(ex))
        wText += "- REMOTE exception: Timeout:\n  {0}".format(ex) + "\n"
        raise RuntimeError()


    # - ToDo: How to get SSL info from the socket?
    """
    if args.verbose >= 1:
        print("- Socket diagnostic:")
        pprint.pprint(socket)
        print("\n")
    """

    # - Just continue.
    sock.settimeout(timeout)

    try:
        der_cert = sock.getpeercert(True)
    finally:
        sock.close()

    return ssl.DER_cert_to_PEM_cert(der_cert)


def get_ssl(hostname):
    global args, wText

    certificate = get_certificate(hostname)
    try:
        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, certificate)
    except RuntimeError as ex:
        raise RuntimeError()

    # - Decode dict values of bytes
    def de_byter(my_dict):
        new_dict = {}
        for key, value in my_dict.items():
            new_dict.update({key.decode('utf-8') : value.decode('utf-8')})
        return new_dict

    # - Reformat a date string, eg:
    #   '20210327120000Z'  ->
    #   '2021-03-27 12:00:00 Z'
    # - ToDo: sanity test: my_str must be 14 characters
    #
    def de_dater(my_str):
        new_str = ''
        new_str += my_str[0:4] + '-'
        new_str += my_str[4:6] + '-'
        new_str += my_str[6:8] + ' '
        new_str += my_str[8:10]  + ':'
        new_str += my_str[10:12] + ':'
        new_str += my_str[12:14] + ' '
        new_str += my_str[14]
        return new_str

    result = {
        'subject': de_byter(dict(x509.get_subject().get_components())),
        'issuer':  de_byter(dict(x509.get_issuer().get_components())),
        'serialNumber': x509.get_serial_number(),
        'version': x509.get_version(),

        'notBefore': de_dater(x509.get_notBefore().decode('utf-8')),
        'notAfter':  de_dater(x509.get_notAfter().decode('utf-8')),
    }

    ## print(f'\n-- diag --\n')
    ##-## extensions = (x509.get_extension(i) for i in range(x509.get_extension_count()))

    extensions = []         # - Empty array
    for i in range(x509.get_extension_count()):
        ge = x509.get_extension(i)
        ## print(f'- pp ge:')
        ## pprint.pprint(ge)
        try:
            ge_str = str(ge)
        except Exception as ex:
            ge_str = "...str(ge) throws exception..."
        ## ge_str = re.sub('[\n\r]', '', ge_str)           # - Remove newline chars
        ## print(f'- extension: {i:2d} : "{ge_str}"')
        extensions.append(ge)

    ## print(f'\n-- mid diag --\n')
    # - Unicode bytes: get_short_name()
    ##-## extension_data = {e.get_short_name().decode('utf-8'): str(e) for e in extensions}

    extension_data = {}     # - empty hash
    for idx, e in enumerate(extensions):
        ## print(f'- extension_data: {idx:2d}')
        short_name = e.get_short_name()
        ## print(f'  - short_name: {short_name}')
        sn_decoded = short_name.decode('utf-8')
        ## print(f'  - sn_decoded: {sn_decoded}')
        try:
            e_str = str(e)
        except Exception as ex:
            e_str = "...str(e) throws exception..."
        ## e_str = re.sub('[\n\r]', '', e_str)             # - Remove newline chars
        ## print(f'  - e_str: {e_str}')
        extension_data[sn_decoded] = e_str

    ## print(f'\n-- end diag --\n')
    result.update(extension_data)

    if args.verbose >= 2:
        print("- Certificate diagnostic:")
        pprint.pprint(result)
        print("")

    return result



# - Query the Fortiguard webfilter
#
def get_fortiguard(domain):
    global args, sleep_loop, wText

    r = None
    try:
        r = requests.get('https://fortiguard.com/webfilter', {'q': domain})
        r.raise_for_status()

    except  ( \
                requests.exceptions.ConnectTimeout,
                requests.exceptions.ReadTimeout,
                requests.exceptions.Timeout,
                requests.exceptions.ConnectionError,
            ) as ex:
        print('  - Error: ', str(ex))
        wText += '  - Error: ', str(ex) + "\n"
        raise RuntimeError()

    except  ( \
                requests.exceptions.HTTPError,
            ) as ex:
        print('  - HTTP error: ' + str(r.status_code))
        print('      ' + str(ex))
        wText += '  - HTTP error: ' + str(r.status_code) + "\n"
        wText += '      ' + str(ex) + "\n"
        raise RuntimeError()

    # - web api success
    raw  = r.text


    """
    # - Sadly, no JSON returned. Must parse the HTML.
    data = r.json()
    ## print("- data:\n" + repr(data) + "\n")

    # - Write the raw result to the JSON file - INCLUDING expired certs.
    #
    if len(data) > 0:
        with open(args.output + '/results.json', 'a') as f2:        # - Append
            # - This line gives formated JSON output, but raw is better for later filtering:
            ## print(json.dumps (data, separators=(",", ": "), sort_keys=True, indent=4), file=f2)
            print(raw, file=f2)
    """


    # - Dump the raw HTML into a file, overwrite previous entries.
    with open(args.output + '/results-fortinet.html', 'a') as f2:   # - Append
        print(raw, file=f2)


    """
    # - Diagnostic output is already written to a file above.
    #   Just one example is enough.
    #
    if args.verbose >= 3:
        pprint.pprint(raw)
        print("\n")
    """

    time.sleep(sleep_loop)
    return r


# - Parse HTML from Fortiguard webfilter to get the Category assessment
# - FYI:
#   $ grep  Category: output/results.html
#   <meta name="description" property="description" content="Category: Shopping" />
#   <meta name="twitter:description" property="twitter:url" content="Category: Shopping" />
#   <meta name="og:description" property="og:description" content="Category: Shopping" />
#   <h4 class="info_title">Category: Shopping</h4>

def get_category(r):
    global args, wText

    category = '- No Category -'
    soup = BeautifulSoup(r.text, 'html.parser')

    """
    # - Nice output, but very verbose, eg: > 900 lines.
    if args.verbose >= 3:
        print("- BeautifulSoup prettify():")
        print(soup.prettify())
        print("\n")
    """

    # - Dump the prettified output into a file, as a diagnostic.
    with open(args.output + '/results-soup.txt', 'a') as f4:        # - Append
        print("- BeautifulSoup prettify():", file=f4)
        print(soup.prettify(), file=f4)
        print("\n", file=f4)


    for item_list in soup.find_all('meta', {"name":"description", "property":"description"}):
        if item_list.has_attr('content'):

            for item in item_list:
                if args.verbose >= 3:
                    print(" - item: " + repr(item))

                item_string = repr(item)        # - ToDo: Is this Unicode compatible?
                match = re.search (r'content="Category:\s+([^"]+)"', item_string, flags=re.IGNORECASE)
                if match:
                    category = match.group(1)
                    break

    # - Category is typically a string like these:
    #       $ grep -i category: output.many/results.txt | sort -u
    #       Category: Arts and Culture
    #       Category: Brokerage and Trading
    #       Category: Business
    #       Category: Education
    #       Category: Finance and Banking
    #       Category: Government and Legal Organizations
    #       Category: Information Technology
    #       Category: Information and Computer Security
    #       Category: Meaningless Content
    #       Category: Newly Observed Domain
    #       Category: Not Rated
    #       Category: Political Organizations
    #       Category: Shopping
    #       Category: Sports
    #       Category: Travel
    #       Category: Web Hosting
    #       Category: Web-based Email
    #
    # - Sometimes, internal errors in Fortinet give strings like this, with long lines:
    #   <meta name="description" property="description"
    #     content="Category: Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException:
    #      http://wfms is unavailable in /var/www/webapp/src/AppBundle/Services/MicroService.php:129
    #      Stack trace:
    #      #0 /var/www/webapp/src/AppBundle/Services/MicroService.php(148): AppBundle\Services\MicroService-&gt;throw_unavailable()
    #    ...
    #
    # - If the category has characters other than "word-letters" and spaces, add a warning.

    match = re.search (r'^[\w\s]+$', category, flags=re.IGNORECASE)
    if not match:
        category = "- Unexpected Category: '" + category[0:75] + "...'\n" + \
                     "                " + \
                     "-> see file: output/results-soup.txt"

    return category




# - Send output to PasteBin
#   https://pastebin.com/api

def send_to_pasteBin(pasteBin_key, wText):
    global args, pasteBin_url

    r = None
    url = '- No URL -'

    # - Use this to create a "named paste" with a dedicated user-access account:
    """
    login_data = {
        'api_dev_key'       : pasteBin_key,
        'api_user_name'     : None,
        'api_user_password' : '',
        'api_paste_private' : 0,
    }
    """

    # - Generic paste parameters:
    data = {
        'api_option'            : 'paste',
        'api_dev_key'           : pasteBin_key,
        'api_paste_code'        : wText,                ## 'John text',
        'api_paste_name'        : 'Cert-Info',
        'api_paste_expire_date' : '1W',
        'api_paste_private'     : 0,
        ## 'api_user_key'       : None,
        'api_paste_format'      : 'bash',
    }


    try:
        """
        login = requests.post(pasteBin_url, data=login_data)
        if args.verbose >= 1:
            print("  - Login status: ", login.status_code if login.status_code != 200 else "OK/200")
            print("  - User token:   ", login.text)
        data['api_user_key'] = login.text
        """

        r = requests.post(pasteBin_url, data=data)
        if args.verbose >= 1:
            print("  - Paste send:   ", r.status_code if r.status_code != 200 else "OK/200")
            print("  - Paste URL:    ", r.text)
            print()

        raw = r.text
        url = raw
        r.raise_for_status()

    except  ( \
                requests.exceptions.ConnectTimeout,
                requests.exceptions.ReadTimeout,
                requests.exceptions.Timeout,
                requests.exceptions.ConnectionError,
            ) as ex:
        print('  - Error: ', str(ex))
        raise RuntimeError()

    except  ( \
                requests.exceptions.HTTPError,
            ) as ex:
        print('  - HTTP error: ', r.status_code)
        print('      ', str(ex))
        raise RuntimeError()

    if (args.verbose >= 1):
        print("  http status: " + str(r.status_code))

    if args.verbose >= 2:
        print("- PasteBin pprint(r):")
        pprint.pprint(r)
        print("\n")

    return url



########
# Main #
########

def main():
    global args, wText

    args = parseArgs()
    domains = loadList(args.file, args.domain)
    if not domains:
        print("- Error, no domains given.\n")
        wText += "- Error, no domains given.\n"
        sys.exit(1)        ## raise RuntimeError()

    wText += "== cert-info.py ("+__version__+") ==" + "\n \n"
    print("- cert-info.py ("+__version__+")")


    # - Create the output directory, if non-existant:
    os.makedirs(args.output, exist_ok=True)

    # - Unconditionally write an empty JSON file:
    open(args.output + '/results.json', 'a')                    # - Append

    if args.unsafe:
        print("- UNSAFE mode is active.")
        wText += "- UNSAFE mode is active." + "\n"
    else:
        print("- SAFE mode is active.")
        wText += "- SAFE mode is active." + "\n"

    for domain in domains:      ## sorted(domains, key=str.casefold):
        print(".\n" + domain)
        wText += "\n" + domain + "\n"


        ####################
        # Certificate info #
        ####################

        try:
            result = get_ssl(domain)
        except RuntimeError:    ## as ex:
            continue

        # - First save a JSON edition - all on one line:
        with open(args.output + '/results.json', 'a') as f2:    # - Append
            print(json.dumps(result, separators=(",", ": "), sort_keys=True, indent=None), file=f2)


        # - Produce output like OpenSSL does, eg:
        #   Issuer:   C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert Global Root CA
        #   Subject:  C=US, O=DigiCert Inc, OU=www.digicert.com, CN=GeoTrust RSA CA 2018
        #
        #   print("- Subject: {}".format( repr (s_dict)))
        #   - Subject: {'C': 'CH', 'ST': 'Aargau', 'L': 'Mägenwil', 'O': 'Competec Holding AG', 'OU': 'IT', 'CN': 'cdn.competec.ch'}

        subject_dict = result.get('subject')
        subject_str  = repr(subject_dict)
        print("  {0:<11s} ".format('Subject:') + (', ').join ('{0}={1}'.format(k, v) for k, v in sorted(subject_dict.items())))
        wText += "  {0:<11s} ".format('Subject:') + (', ').join ('{0}={1}'.format(k, v) for k, v in sorted(subject_dict.items())) + "\n"

        issuer_dict = result.get('issuer')
        issuer_str  = repr(issuer_dict)
        print("  {0:<11s} ".format('Issuer:')  + (', ').join ('{0}={1}'.format(k, v) for k, v in sorted(issuer_dict.items())))
        wText += "  {0:<11s} ".format('Issuer:')  + (', ').join ('{0}={1}'.format(k, v) for k, v in sorted(issuer_dict.items())) + "\n"

        not_before = result.get('notBefore')
        print("  {0:<11s} {1}".format('notBefore:', not_before))
        wText += "  {0:<11s} {1}".format('notBefore:', not_before) + "\n"

        not_after = result.get('notAfter')
        print("  {0:<11s} {1}".format('notAfter:',  not_after))
        wText += "  {0:<11s} {1}".format('notAfter:',  not_after) + "\n"

        san_str = result.get('subjectAltName')
        san_list = []

        if san_str:
            san_list = sorted(san_str.split(','))

        if len(san_list) == 0:
            print("  {0:<11s} {1}".format('SAN:', '- None -'))
            wText += "  {0:<11s} {1}".format('SAN:', '- None -') + "\n"
        else:
            print("  {0:<11s} {1}".format('SAN:', san_list[0].strip().rstrip()))
            wText += "  {0:<11s} {1}".format('SAN:', san_list[0].strip().rstrip()) + "\n"

            for i in range(1,len(san_list)):
                print("  {0:<11s} {1}".format('', san_list[i].strip().rstrip()))
                wText += "  {0:<11s} {1}".format('', san_list[i].strip().rstrip()) + "\n"


        #####################
        # Security Analysis #
        #####################

        print("  ** Analysis")
        wText += "  ** Analysis" + "\n"

        good = True
        print("    Expired?         ", end="")
        wText += "    Expired?         "

        after_obj = datetime.datetime.strptime(not_after, "%Y-%m-%d %H:%M:%S Z")   # - ToDo: UTC
        now_obj   = datetime.datetime.now()                                        # - ToDo: Localtime
        diff      = now_obj - after_obj
        good = (True if diff.total_seconds() < 0 else False)
        if good:
            print("No  [OK]")
            wText += "No  [OK]" + "\n"
        else:
            print("Yes [NOK]  -> {:0>8}".format( str(datetime.timedelta(seconds=int(diff.total_seconds()) ))))
            wText += "Yes [NOK]  -> {:0>8}".format( str(datetime.timedelta(seconds=int(diff.total_seconds()) ))) + "\n"

        # - I consider it bad practice, if the same cert is allowed BOTH as a server-cert and client-cert.
        good = True
        print("    Purpose overlap? ", end="")
        wText += "    Purpose overlap? "

        eku = result.get('extendedKeyUsage')
        if eku:
            match_server_use = re.search (r'\sServer\sAuth', eku, flags=re.IGNORECASE)
            match_client_use = re.search (r'\sClient\sAuth', eku, flags=re.IGNORECASE)
            if match_server_use and match_client_use:
                good = False
        if good:
            print("No  [OK]")
            wText += "No  [OK]" + "\n"
        else:
            print("Yes [NOK]  -> extended_Key_Usage: " + eku)
            wText += "Yes [NOK]  -> extended_Key_Usage: " + eku + "\n"

        print("    Self-signed?     " + ("Yes [NOK]" if subject_str == issuer_str else "No  [OK]"))
        wText += "    Self-signed?     " + ("Yes [NOK]" if subject_str == issuer_str else "No  [OK]") + "\n"


        ###################
        # Fortiguard info #
        ###################

        print("\n- Fortiguard")
        wText += "\n- Fortiguard:" + "\n"

        r = None
        try:
            r = get_fortiguard(domain)
        except RuntimeError:    ## as ex:
            continue

        if (r.status_code != 200) or (args.verbose >= 1):
            print("  http status: " + str(r.status_code))
            wText += "  http status: " + str(r.status_code) + "\n"

        category = get_category(r)
        print("  Category:   " + category)
        wText += "  Category:   " + category + "\n"

        ## pprint.pprint(fg)

    print()
    wText += " \n"


    ############
    # PasteBin #
    ############

    print(".\n- PasteBin")
    wText += ".\n- PasteBin" + "\n"

    if 'PASTEBIN_KEY' in os.environ:
        pasteBin_key = os.environ['PASTEBIN_KEY']
    else:
        pasteBin_key = ''

    if not args.pastebin:
        print("  -> Skipped.")
        wText += "  -> Skipped." + "\n"

    elif not pasteBin_key:
        print("  -> Skipped. Environment variable PASTEBIN_KEY is not set.")
        wText += "  -> Skipped. Environment variable PASTEBIN_KEY is not set." + "\n"

    else:
        wText += "  -> send_to_pasteBin (pasteBin_key, wText)" + "\n"
        try:
            url = send_to_pasteBin(pasteBin_key, wText)
            print("  {0}".format(url))
        except RuntimeError:    ## as ex:
            pass


    #######
    # Log #
    #######

    wText += " \n \n"   # - Extra newlines

    # - Write a text file, with MS-Windows line-endings; CRLF
    with open(args.output + '/results.txt', 'a') as f3:         # - Append
        print(wText.replace("\n", "\r\n"), end='', file=f3)

    print()


if __name__ == '__main__':
    main()
