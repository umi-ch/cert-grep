#!/usr/bin/env bash
set -eo pipefail

# - Show what's exported from the CI Pipeline Runner.

echo "Show environment variables"
echo "- ENV_VAR_A: '"${ENV_VAR_A}"'"
echo "- ENV_VAR_B: '"${ENV_VAR_B}"'"
echo "- ENV_VAR_C: '"${ENV_VAR_C}"'"

